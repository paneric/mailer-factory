<?php

namespace Paneric\Mailer;

use Paneric\Interfaces\Guard\GuardInterface;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class MailerService
{
    public function __construct(
        private readonly GuardInterface $guard,
        private readonly PHPMailer $mailer,
        private readonly array $config,
        private readonly string $local
    ) {
    }

    public function setProcessId(): string
    {
        return $this->guard->generateRandomString($this->config['random_string_length']);
    }

    /**
     * @throws Exception
     */
    public function sendProcessLink(string $process, string $processId, string $userId, string $email): void
    {
        $messages = $this->config['mail_content'];

        $this->mailer->addAddress($email);
        $this->mailer->Subject = $messages[$process . '_email_subject'][$this->local];
        $this->mailer->Body = sprintf(
            $messages[$process . '_email_message'][$this->local],
            $this->config[$process . '_time_delay'],
            $this->config[$process . '_url'],
            $userId,
            $this->guard->hash($processId)
        );

        if (!$this->mailer->send()) {
            throw new Exception($this->mailer->ErrorInfo);
        }
    }
}
