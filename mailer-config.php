<?php

return [
    'charset' => 'UTF-8',
    'encoding' => 'base64',
    'smtp_debug' => 2,
    'host' => 'smtp.gmail.com',
    'smtp_auth' => true,
    'username' => 'gmail@gmail.com',
    'password' => 'password',
    'smtp_secure' => 'tls',
    'port' => 587,
    'html' => true,
    'set_from_address' => 'gmail@gmail.com',
    'set_from_name' => 'Mailer',
];
