<?php

return [
    'random_string_length' => 128,

    'activate_url' => 'http://127.0.0.1:81/auth-api/activate',
    'activation_time_delay' => 24,

    'reset_password_url' => 'http://127.0.0.1:81/auth-api/reset-password',
    'reset_time_delay' => 24,

    'mail_content' => [
        'activate_email_subject' => [
            'en' => 'Account activation automatic message.'
        ],
        'activate_email_message' => [
            'en' => 'Please click the following link within %sh to activate your account: %s/%s/%s/activate .'
        ],
        'reset_password_email_subject' => [
            'en' => 'Password reset automatic message.'
        ],
        'reset_password_email_message' => [
            'en' => 'Please click the following link within %sh to reset your account password: %s/%s/%s/reset .'
        ],
    ],
];
