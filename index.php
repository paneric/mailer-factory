<?php

declare(strict_types=1);

require 'vendor/autoload.php';

use Paneric\Mailer\MailerFactory;

$mailerFactory = new MailerFactory();

$mailerConfig = require 'mailer-config.php';

$mailer = $mailerFactory->create($mailerConfig);
