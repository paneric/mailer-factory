<?php

declare(strict_types=1);

namespace Paneric\Mailer;

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class MailerFactory
{
    public function create(array $configMailer): PHPMailer
    {
        $phpMailer = new PHPMailer();

        $phpMailer->CharSet = $configMailer['charset'];
        $phpMailer->Encoding = $configMailer['encoding'];

        $phpMailer->isSMTP();                                  // Set mailer to use SMTP

        $phpMailer->SMTPDebug = $configMailer['smtp_debug'];   // Enable verbose debug output
        $phpMailer->Host = $configMailer['host'];              // Specify main and backup SMTP servers
        $phpMailer->SMTPAuth = $configMailer['smtp_auth'];     // Enable SMTP authentication
        $phpMailer->Username = $configMailer['username'];      // SMTP username
        $phpMailer->Password = $configMailer['password'];      // SMTP password
        $phpMailer->SMTPSecure = $configMailer['smtp_secure']; // Enable TLS encryption, `ssl` also accepted
        $phpMailer->Port = $configMailer['port'];              // TCP port to connect to

        $phpMailer->isHTML($configMailer['html']);             // Set mailer to use SMTP

        try {
            $phpMailer->setFrom($configMailer['set_from_address'], $configMailer['set_from_name']);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return $phpMailer;
    }
}
